<?php

if ($_POST["cert_email"] != '' && $_POST["cert_name"] != '' && $_POST["cert_phone"] != ''  && $_POST["cert_address"] != '') {
    $path = $_SERVER['DOCUMENT_ROOT'];
    include_once $path . '/wp-config.php';
    include_once $path . '/wp-load.php';
    include_once $path . '/wp-includes/wp-db.php';
    include_once $path . '/wp-includes/pluggable.php';

    $admin_email = $wpdb->get_results( "SELECT `option_value` FROM `wp_options` where `option_name`='admin_email'" );
    $email_to = $admin_email[0]->option_value;

    $name     =   $_POST['cert_name'];
    $email    =   $_POST['cert_email'];
    $phone    =   $_POST['cert_phone'];
    $address  =   $_POST['cert_address'];
    $text     =   $_POST['cert_message'];
    $subject  =   "Заказ подарочного сертификата";

    if ($text == '')
        $text = '---';

    $message = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/><title>' . htmlentities($subject,ENT_COMPAT,'UTF-8') . '</title></head>';
    $message .= '<body style="background-color: #ffffff; color: #000000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 18px; font-family: helvetica, arial, verdana, sans-serif;">';
    $message .= '<h2 style="background-color: #eeeeee; padding: 10px;">' . $subject . '</h2><table cellspacing="0" cellpadding="0" width="100%" style="background-color: #ffffff;">'; 
    $message .= '<tr><td valign="top"><b>Имя:</b></td><td>' . htmlentities($name, ENT_COMPAT,'UTF-8') . '</td></tr>';
    $message .= '<tr><td valign="top"><b>Электронная почта:</b></td><td>' . htmlentities($email,ENT_COMPAT,'UTF-8') . '</td></tr>';
    $message .= '<tr><td valign="top"><b>Сообщение:</b></td><td>' . htmlentities($text,ENT_COMPAT,'UTF-8') . '</td></tr>';
    $message .= '<tr><td valign="top"><b>Сотовый телефон:</b></td><td>' . htmlentities($phone,ENT_COMPAT,'UTF-8') . '</td></tr>';
    $message .= '<tr><td valign="top"><b>Домашний адрес:</b></td><td>' . htmlentities($address,ENT_COMPAT,'UTF-8') . '</td></tr>';

    $message .= '</table><br/><br/>';
    $message .= '</body></html>';


    $name_in_head = '"Certificate PandoRoom" <certificate@pandoroom.ru>';
    $headers  = "From: $name_in_head\r\n";
    $headers .= "Content-Type: text/html; charset=utf-8\r\n";
    $good = mail($email_to, $subject, $message, $headers);

    if ($good) {
        echo "sent";
    } else {
        echo "error";
    }
}
?>