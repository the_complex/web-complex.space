<?php
/*
Plugin Name: Booking plugin for Pandoroom
Plugin URI: 
Description: Система бронирования квестов для проекта Pandoroom
Version: 1.0
Author: 
Author URI: 



This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function show_page_quest($a)
{ 
 //$tit = get_admin_page_title();
 //$uf  = explode("#",$tit);
 //$idq = trim($uf[1]);
 //print $idq;
 
 $ida=$_GET['page'];
 include_once("alldays.inc");
}



function new_comments_page()
 {

	add_menu_page(null, 'Бронирование', 'manage_options', 'pandoroom-booking/admin.php', '', 'dashicons-calendar-alt', 18);
   	
}
add_action( 'admin_menu', 'new_comments_page' );


function new_alldays_page()
 {
  $questslist=get_all_quests();
  for($z=0;$z<count($questslist);$z++)
   { 
		add_submenu_page('pandoroom-booking/admin.php', 'Квест #'.$questslist[$z]->ID, $questslist[$z]->post_title, 'manage_options', $questslist[$z]->ID, 'show_page_quest');
	//	add_submenu_page('pandoroom-booking/admin.php', 'Квест #'.$questslist[$z]->ID, $questslist[$z]->post_title, 'manage_options', 'pandoroom-booking/alldays.php', '');
   }
}
add_action( 'admin_menu', 'new_alldays_page' );

function new_control_page(){
    add_submenu_page( 
        'pandoroom-booking/admin.php', 'Статистика', 'Статистика', 'manage_options', 'pandoroom-booking/moderate.php', ''
    );
}
add_action( 'admin_menu', 'new_control_page' );

