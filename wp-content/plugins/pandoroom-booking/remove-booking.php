<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$cancel = intval( $_GET["cancel"] );
$client_id = intval( $_GET["id"] );

$r = is_int( $cancel );
$c = is_int( $client_id );

$query = "DELETE FROM wp_pand_booking WHERE id = '$cancel'";

if ( $r && $c ) {
	$result = $wpdb->query( $query );
	if ( !$_GET["view"] ) {
		echo $result;
	} else {
		if ($result) {
			echo 'Бронь успешно аннулирована!';
		} else {
			echo 'Ошибка удаления брони...';
		}
	}
}