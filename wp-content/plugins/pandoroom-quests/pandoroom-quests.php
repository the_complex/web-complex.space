<?php
/*
Plugin Name: Pandoroom quests
Plugin URI: 
Description: Плагин для администрирования квестов Pandoroom
Version: 1.0
Author: 
Author URI: 
License: GPLv2
*/

add_action( 'init', 'init_quests' );

function init_quests() {
    register_post_type( 'room',
        array(
            'labels' => array(
                'name'					=> 'Квесты',
                'singular_name'			=> 'Квест',
                'add_new'				=> 'Добавить квест',
                'add_new_item'			=> 'Добавить новый квест',
                'edit'					=> 'Редактировать',
                'edit_item'				=> 'Редактировать квест',
                'new_item'				=> 'Новый квест',
                'view'					=> 'Просмотреть',
                'view_item'				=> 'Просмотреть квест',
                'search_items'			=> 'Найти квесты',
                'not_found'				=> 'Квестов не найдено',
                'not_found_in_trash'	=> 'Нет квестов в корзине',
                'parent'				=> 'Родительский квест'
            ),
            'public'		=> true,
            'menu_position'	=> 15,
            'supports'		=> array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
            'taxonomies'	=> array( '' ),
            'menu_icon'		=> 'dashicons-editor-unlink',
            'has_archive'	=> true
        )
    );
}



add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    if ( get_post_type() == 'room' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-quests.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/single-quests.php';
            }
        }
    }
    return $template_path;
}