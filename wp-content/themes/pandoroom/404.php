<?php get_header(); ?>

<section id="content">
	<h1>Ошибка 404 - Страница не найдена!</h1>
	<p>Извините, запрашиваемая вами страница не существует</p>
	<?php get_search_form(); ?>
</section>

<?php get_footer(); ?>