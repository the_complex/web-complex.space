<?php
	$top_menu_args = array(
	  'menu'            => 'top_menu', 
	  'container'       => 'nav',
	  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s<li class="clear"></li></ul>',
	);

	if ($_POST["r_email"] != '') {
		new_user_registration();
	}

	if ( is_user_logged_in() ) {
		global $current_user;
		$current_user = wp_get_current_user();
		$user_name = $current_user->display_name;
	}

    //print_r($_SERVER);
?>

<!DOCTYPE html>
<head>
	<?= cp(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
	<meta name="viewport" content="width=1060">
<meta name="description" content="">
<meta name="p:domain_verify" content="195fcb24eb77bbc896ccb70120cc5818" /> 
	<meta name="author" content="<?php bloginfo('name'); ?>">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" />
	<title><?php wp_title('&raquo;', true, 'right'); ?><?php bloginfo('name'); ?></title>
	<!--[if lt IE 9]> 
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<![endif]-->
	<!-- Theme styles -->
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
	<link href="<?php bloginfo('template_directory'); ?>/js/fancy/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php bloginfo('template_directory'); ?>/js/fancy/helpers/jquery.fancybox-thumbs.css?v=1.0.7" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php bloginfo('template_directory'); ?>/js/fancy/helpers/jquery.fancybox-buttons.css?v=1.0.5" rel="stylesheet" type="text/css" media="screen" /> 
	<link href="<?php bloginfo('template_directory'); ?>/js/owl/owl.carousel.css" rel="stylesheet" />
	<link href="<?php bloginfo('template_directory'); ?>/js/owl/owl.theme.css" rel="stylesheet" />
	<link href="<?php bloginfo('template_directory'); ?>/css/jquery.bxslider.css" rel="stylesheet" /> 
	<!-- Theme styles end -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php check_success_registration(); ?>
	<div id="container">
		<header class="clearfix"><!--
			<div class="centered">
				<div class="header_buttons">
					<a href="<?= get_permalink(57); ?>" class="select_city">Владивосток</a>
					<?php if ( is_user_logged_in() ) { ?>
						<a href="<?= get_permalink(18); ?>" class="log_in"><?= $user_name; ?></a>
					<?php } else { ?>
						<a href="#login_register" class="log_in">Вход</a>
					<?php } ?>
				</div>
				<a href="/" class="logo header_home_link"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"></a>
				<?php print_header_phone(); ?>
				<?php wp_nav_menu($top_menu_args); ?>
			</div>
			<div class="header_border_bottom"></div>-->
		</header>