<?php 

if (isset($_GET["date"]) && $_GET["date"] != '' ) {
	$quest = get_single_quest($post->ID);
	print_quest_schedule( $post->ID, $_GET["date"] );

} else {

$quest = get_single_quest($post->ID);
get_header();

?>

<section id="content">
	<div class="quest_details clearfix">
		<div class="centered">
			<figure class="quest_photo" style="background-image: url(<?= $quest->cfs["image"]["url"] ?>);"></figure>
			<div class="single_quest_info clearfix">
				<div class="sqi_left">
					<?php
						echo '<div class="logo_players clearfix">
			                            <figure class="quest_logo" style="background-image: url(', $quest->cfs["icon"]["url"], ');"></figure>
			                            <span class="players_icons">';
			                            $min_players = intval( $quest->cfs['min_players'] );
			                            $max_players = intval( $quest->cfs['max_players'] );
			                            echo get_quest_players( $min_players, $max_players );
			                        echo '</span>
			                            <span class="players_text">', $quest->cfs['min_players'], '-', $quest->cfs['max_players'], ' игрока</span>
			                        </div>
			                        <span class="raiting">';
			                                $stars = intval( $quest->cfs['raiting'] );
			                                echo get_quest_stars( $stars );
			                    echo '</span>';
					?>
				</div>
				<div class="sqi_right">
					<span class="sq_phone"><?= $quest->cfs['phone']; ?></span>
					<a href="mailto:<?= $quest->cfs['email']; ?>"  class="sq_email"><?= $quest->cfs['email']; ?></a>
					<span class="sq_address"><?= $quest->cfs['address']; ?></span>
				</div>
				<div class="clear"></div>
				<h1 class="single_quest_title"><?= $quest->post_title; ?></h1>
				<div class="single_quest_description">
					<?= $quest->cfs["description"]; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="centered">
		<div class="come_with_friends">
			<span class="choose_date">Выберите дату и время</span><br>
			<span class="and_come_with">и приходите с друзьями</span>
		</div>

				<div class="games_calendar">
		    <div><span class="date_today_title">сегодня</span></div>
			<span class="date_today"><?= get_today_date(); ?></span>
			<img src="<?php bloginfo('template_directory'); ?>/img/calendar.png" alt="" class="calendar_image">
			<a href="/room/<?=$all_quests[$rs]->post_name?>"><?=$all_quests[$rs]->post_title;?></a>
			
			<table border="0" width="100%" cellpadding="10" id="table1">
	     		<?= get_next_dates2($max_dates,$quest->ID); ?>
	         </table>

	<BR />
	<BR />
	
	</div>
			<div><span class="selected_date">Выбранная дата: <span data="<?php echo date("Y-m-d"); ?>"><?= get_today_date(); ?></span></span></div>
		</div>
	</div>
	<div class="quests_schedule">
		<?php
			$date = date("Y-m-d");
			print_quest_schedule2( $post->ID, $date );
		?>
	</div>
	<br/>
	<br/>
	<br/>
	<br/>
<div><ul class="quests_list2"><li class="r">
              <?php
			  
	$a = $quest->cfs['foto1'];
	
				if (empty($a)) {
    echo '';
}
				else {
        echo ('<a href="http://pandoroom.org/galereya-kvesta-psihbolnitsa-2" class="single_quest2" style="background-image: url('. $quest->cfs["foto1"]["url"]. ');">
                        <div class="logo_players clearfix">
                           
                       
                       </div>
                      <span class="quest_title2">'. $quest->cfs["ms1"]. '</span>
                       <div class="quest_bottom_info2">
                            <span class="sq_phone">'. $quest->cfs["vremya1"]. '</span>
                            <span class="sq_email">'. $quest->cfs["komand1"]. '</span>
                           
                        </div>
                    </a>');
                 }
				    ?>       
			 
                </li>
				<li class="r">
				              <?php
			  
	$a = $quest->cfs['foto2'];
	
				if (empty($a)) {
    echo '';
}
				else {
        echo ('<a href="http://pandoroom.org/galereya-kvesta-psihbolnitsa-2" class="single_quest2" style="background-image: url('. $quest->cfs["foto2"]["url"]. ');">
                        <div class="logo_players clearfix">
                          
                       
                       </div>
                       <span class="quest_title2">'. $quest->cfs["ms2"]. '</span>
                       <div class="quest_bottom_info2">
                            <span class="sq_phone" style>'. $quest->cfs["vremya2"]. '</span>
                            <span class="sq_email">'. $quest->cfs["komand2"]. '</span>
                           
                        </div>
                    </a>');
                 }
				    ?>       
				</li>
				<li class="r"> 
				              <?php
			  
	$a = $quest->cfs['foto3'];
	
				if (empty($a)) {
    echo '';
}
				else {
        echo ('<a href="http://pandoroom.org/galereya-kvesta-psihbolnitsa-2" class="single_quest2" style="background-image: url('. $quest->cfs["foto3"]["url"]. ');">
                        <div class="logo_players clearfix">
                            
                       </div>
                       <span class="quest_title2">'. $quest->cfs["ms3"]. '</span>
                       <div class="quest_bottom_info2">
                            <span class="sq_phone">'. $quest->cfs["vremya3"]. '</span>
                            <span class="sq_email">'. $quest->cfs["komand3"]. '</span>
                           
                        </div>
                    </a>');
                 }
				    ?>       
				</li>
				</ul>
              </div>
				
</section>

<?php get_footer(); ?>

<?php } ?>