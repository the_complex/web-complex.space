<?php get_header(); ?>

<section id="content">

	<article id="publication" class="internal">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
				<small><?= rus_date($post->ID) ?></small>
					<?php the_content('Читать далее'); ?>
	
				<p class="postmetadata">Posted in <?php the_category(', ') ?> by <?php the_author(); ?> <br />
				<?php the_tags( 'Tags: ', ', ', ''); ?>
					<?php comments_popup_link('No Comments', '1 Comment', '% Comments');?> </p>
					<?php comments_template(); ?>
						<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
		<?php endwhile; ?>
	
		<?php next_posts_link('Старые статьи') ?> <?php previous_posts_link('Новые статьи') ?>
	<?php endif; ?>
	</article>

</section>

<?php get_footer(); ?>