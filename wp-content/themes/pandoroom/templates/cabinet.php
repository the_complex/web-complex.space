<?php

/*
Template name: Кабинет
*/

// если пользователь не авторизован редиректим его на главную страницу
if (!is_user_logged_in()) {
	header('Location: ' . home_url());
}

//print_r( $_SERVER );
// $_SERVER["REQUEST_TIME"];

get_header();

global $current_user;
$user_info = get_userdata($current_user->ID);

global $wpdb;
$qstr = "SELECT * FROM wp_pand_booking WHERE client_id = $current_user->ID ORDER BY book_time";
$user_books = $wpdb->get_results($qstr);

?>

<section id="content">
	<div class="centered">
		<?php
			//print_r($current_user);
		?>
		<p class="cab_info">В личном кабинете хранятся ваши контакты, планируемые квесты, а также история.</p>
		<div class="user_info clearfix">
			<figure class="user_image" style="background-image: url(<?= bloginfo("template_directory") ?>/img/111.png)"></figure>
			<div>
				<span class="user_name"><?= $current_user->display_name; ?></span><br>
				<span class="user_phone"><?= $user_info->phone; ?></span><br>
				<span class="user_email"><?= $current_user->user_email; ?></span><br>
				<a href="<?php echo wp_logout_url( home_url() ); ?>" title="Выход из профиля">Выход</a>
			</div>
		</div>
		<div></div><br>
		<div>
			<?php
				if ( count($user_books) ) {
					$i = 0;
					$j = 0;
					foreach ($user_books as $book) {
						if ( strtotime( $book->book_time ) < $_SERVER["REQUEST_TIME"] ) {
							$last_games[] = $book;
						} else {
							$booked_games[] = $book;
						}
					}

					if ( count( $booked_games ) ) {
						echo '<h1 class="booked">Забронированное время</h1>
								<ul class="games clearfix">';
						foreach ($booked_games as $game) {
							$quest = get_post($game->location_id);
							$quest->cfs = get_fields( $quest->ID );
							print_cabinet_quest($quest, $game);
						}
						echo '</ul>';
					}
					if ( count( $last_games ) ) {
						echo '<h1 class="booked">История</h1>
								<ul class="games last_games clearfix">';
						foreach ($last_games as $game) {
							$quest = get_post($game->location_id);
							$quest->cfs = get_fields( $quest->ID );

							print_cabinet_quest( $quest, $game, 1 );
						}
						echo '</ul>';
					}
				} else {
			?>
			<div class="no_games">
				<p>Вы пока не бронировали, и не прошли ни одного квеста. А зря... У нас очень интересно.
					Попробуйте, вы не пожалеете!</p>
				<p>Выбрать интересный вам квест можно на <a href="<?= home_url(); ?>">главной странице</a>.</p>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>