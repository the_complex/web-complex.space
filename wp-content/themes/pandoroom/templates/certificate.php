<?php

/*
Template name: Сертификат
*/

if ( is_user_logged_in() ) {
	global $current_user;
	$user_info = get_userdata($current_user->ID);
}

$cert_fields = get_fields($post->ID);

get_header(); ?>

<section id="content">
	<div class="centered clearfix certificate_page">
		<div class="cert_success">
			Заказ сертификата успешно оформлен. В течение ближайшего времени с Вами свяжется наш сотрудник для уточнения деталей заказа.<br>Спасибо за то, что воспользовались нашими услугами.
		</div>
		<div class="cert_error">
			При заказе сертификата обнаружена ошибка. Попробуйте заказать еще раз позже...
		</div>
		<div class="certificate_left_info">
			<?= $cert_fields["cert_description"]; ?>
		</div>
		<div class="certificate_form_wrapper">
			<form method="post" action="<?= get_permalink($post->ID); ?>" class="certificate_order">
				<input type="text" class="input" id="cert_name" name="cert_name" required="required" placeholder="Ваше имя"<?php if( is_user_logged_in() ) { echo 'value="', $current_user->display_name, '"'; } ?>>
				<input type="email" class="input" id="cert_email" name="cert_email" required="required" placeholder="E-mail"<?php if( is_user_logged_in() ) { echo 'value="', $current_user->user_email, '"'; } ?>>
				<input type="text" class="input" name="cert_phone" id="cert_phone" required="required" placeholder="Введите номер телефона"<?php if( is_user_logged_in() ) { echo 'value="', $user_info->phone, '"'; } ?>>
				<input type="text" class="input" name="cert_address" id="cert_address" required="required" placeholder="Введите адрес">
				<textarea name="cert_message" id="cert_message" class="textarea" placeholder="Добавить краткое сообщение..."></textarea>
				<input type="submit" class="button submit" id="button_order_certificate" name="button_order_certificate" value="Заказать">
			</form>
		</div>
	</div>
</section>

<?php get_footer(); ?>