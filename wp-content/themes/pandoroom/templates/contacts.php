<?php

/*
Template name: Контакты
*/

get_header(); ?>

<section id="content">
	<div class="centered">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>